# Yoco Payment UI SDK - Android

Installation
-------
Please see our installation guide at [Documentation](https://developer.yoco.com/in-person/android/installation-android)

Author
------
Yoco, support@yoco.com

License
-------
YocoSDK is available under the Apache 2.0 license. See the [License](https://gitlab.com/yoco/yoco-payment-sdk-android-ui/-/raw/develop/LICENSE) file for details.