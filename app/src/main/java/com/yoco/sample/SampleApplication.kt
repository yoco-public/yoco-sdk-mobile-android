package com.yoco.sample

import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.multidex.MultiDexApplication
import com.yoco.payments.sdk.YocoSDK
import com.yoco.payments.sdk.data.enums.SDKEnvironment

class SampleApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        // Initialises Yoco Payment UI SDK
        YocoSDK.initialise(
            context = applicationContext
        )
        // Configures the Yoco Payment UI SDK
        YocoSDK.configure(
            context = applicationContext,
            secret = "s-ecret",
            environment = SDKEnvironment.PRODUCTION,
            enableLogging = true
        )
    }
}