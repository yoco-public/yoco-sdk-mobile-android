package com.yoco.sample

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.yoco.payments.sdk.YocoSDK
import com.yoco.payments.sdk.data.enums.SDKEnvironment
import com.yoco.sample.databinding.SampleActivityBinding

class SampleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<SampleActivityBinding>(this, R.layout.sample_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SampleFragment.newInstance())
                .commitNow()
        }
        initialiseSdk()
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    fun initialiseSdk() {
        // Initialises Yoco Payment UI SDK
        YocoSDK.initialise(
            context = applicationContext
        )
        // Configures the Yoco Payment UI SDK
        YocoSDK.configure(
            context = applicationContext,
            secret = "s-ecret",
            environment = SDKEnvironment.PRODUCTION,
            enableLogging = true
        )
    }
}