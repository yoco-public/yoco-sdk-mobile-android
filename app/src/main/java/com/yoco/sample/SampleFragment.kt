package com.yoco.sample

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.motion.widget.TransitionBuilder.validate
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.yoco.sample.R
import com.yoco.payments.sdk.YocoSDK
import com.yoco.payments.sdk.data.YocoStaff
import com.yoco.payments.sdk.data.delegates.DefaultReceiptDelegate
import com.yoco.payments.sdk.data.delegates.ReceiptDelegate
import com.yoco.payments.sdk.data.delegates.UpdatePrintProgress
import com.yoco.payments.sdk.data.delegates.UpdateReceiptProgress
import com.yoco.payments.sdk.data.enums.*
import com.yoco.payments.sdk.data.params.MerchantInfo
import com.yoco.payments.sdk.data.params.PaymentParameters
import com.yoco.payments.sdk.data.params.PrintParameters
import com.yoco.payments.sdk.data.params.PrinterConfig
import com.yoco.payments.sdk.data.params.PrinterProvider
import com.yoco.payments.sdk.data.params.RefundParameters
import com.yoco.payments.sdk.data.params.TippingConfig
import com.yoco.payments.sdk.data.result.PaymentResult
import com.yoco.payments.sdk.data.result.PaymentResultInfo
import com.yoco.payments.sdk.data.result.PrintResult
import com.yoco.payments.sdk.data.result.PrinterStatus
import com.yoco.sample.databinding.SampleFragmentBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.HashMap

class SampleFragment : Fragment() {

    companion object {
        fun newInstance() = SampleFragment()
    }

    private val receiptDelegate = DefaultReceiptDelegate()

    private var tippingConfig: TippingConfig = TippingConfig.DO_NOT_ASK_FOR_TIP
    private lateinit var mBinding: SampleFragmentBinding

    private val refundTransactionId = MutableLiveData("")
    private val paymentResult = MutableLiveData<PaymentResult?>(null)
    private var receiptNumber = MutableLiveData("")
    private var merchantInfo: MerchantInfo =
        MerchantInfo("merchant-id", "merchant-name", "123-456-789", "fake address")
    private var printerConfig = PrinterConfig(printerProvider = PrinterProvider.NONE)
    private var printSuccess = false
    private fun getPrintParameters() = PrintParameters(
        clientTransactionId = null,
        merchantInfo = this.merchantInfo,
        transactionType = TransactionType.GOODS,
        signature = null,
        printStartListener = ::printStartListener,
        printerStatusListener = ::printerStatusUpdate,
        printResultListener = ::printResultUpdate,
        metadata = null
    )

    // Charge result contract
    private val chargeResult: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            System.err.println("Result Code: ${result.resultCode}") // PaymentSDK.Response.ResultCode
            val transactionResult: PaymentResult? =
                result.data?.getSerializableExtra(PaymentResultInfo.ResultKeys.Transaction) as PaymentResult?
            System.err.println("Transaction Result: $transactionResult")
            refundTransactionId.value = transactionResult?.clientTransactionId
        }

    // Refund result contract
    private val refundResult: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            System.err.println("Result Code: ${result.resultCode}") // PaymentSDK.Response.ResultCode
            val refundResult: PaymentResult? =
                result.data?.getSerializableExtra(PaymentResultInfo.ResultKeys.Transaction) as PaymentResult?
            System.err.println("Transaction Result: $refundResult")
            refundTransactionId.value = ""
        }
    // Pairing result constract
    private val pairingResult: ActivityResultLauncher<Intent> = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        System.err.println("Pairing Activity: Result Code: ${result.resultCode}") // PaymentSDK.Response.ResultCode
    }

    // Last transaction lookup result contract
    private val lastTransactionResult: ActivityResultLauncher<Intent> =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            System.err.println("Result Code: ${result.resultCode}") // PaymentSDK.Response.ResultCode
            val lastTransaction: PaymentResult? =
                result.data?.getSerializableExtra(PaymentResultInfo.ResultKeys.Transaction) as PaymentResult?
            System.err.println("Last Transaction Result: $refundResult")
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = SampleFragmentBinding.inflate(layoutInflater)
        // this enables MutableLiveData to be update on your UI
        mBinding.lifecycleOwner = this
        return mBinding.root
    }

    override fun onResume() {
        super.onResume()
        mBinding.chargeField.editText?.setText("")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.receiptNumber = receiptNumber
        configurePrinting()
        setupPairCardMachine()
        setupChargeCard()
        setupTippingConfig()
        setupLastTransaction()
        setupShowTransaction()
        setupIntegratorTransaction()
        setupPrintProvider()
        setupPrintSuccessSwitch()
        setupRefund()
        setupLogout()
    }

    private fun configurePrinting() {
        YocoSDK.setPrinterConfig(printerConfig)
    }

     private fun setupLastTransaction() {
         mBinding.paymentResult = paymentResult
         mBinding.buttonLastTransaction.setOnClickListener {
             val transactionIdStr = refundTransactionId.value
             if (!transactionIdStr.isNullOrBlank()) {
                 YocoSDK.getPaymentResult(transactionIdStr) { resultCode: Int, result: PaymentResult?, errorMessage: String? ->
                     paymentResult.postValue(result)
                     when {
                         result != null -> System.err.println("Last Transaction Result: $resultCode: has result")
                         errorMessage.isNullOrBlank() -> System.err.println("Last Transaction Result: $resultCode: $errorMessage")
                         else -> System.err.println("Last Transaction Result: $resultCode: does not have result")
                     }
                 }
             }
         }
     }

    private fun setupIntegratorTransaction() {
        mBinding.buttonIntegratorTransaction.setOnClickListener {
            receiptNumber.value?.let {
                YocoSDK.getIntegratorTransactions(receiptNumber = it) { resultCode: Int, result: List<PaymentResult>?, errorMessage: String? ->
                    when {
                        result != null -> System.err.println("Integrator Transaction Result: $resultCode: has ${result.size} results ")
                        errorMessage.isNullOrBlank() -> System.err.println("Last Transaction Result: $resultCode: $errorMessage")
                        else -> System.err.println("Last Transaction Result: $resultCode: does not have result")
                    }
                }
            }
        }
    }

     private fun setupShowTransaction() {
         mBinding.buttonShowResult.setOnClickListener {
             val paymentResult = paymentResult.value
             if (paymentResult != null) {
                 val params = PaymentParameters(
                     receiptDelegate = receiptDelegate,
                     transactionCompleteNotifier = null
                 )
                 YocoSDK.showPaymentResult(
                     context = requireContext(),
                     paymentResult = paymentResult,
                     paymentParameters = params,
                     printParameters = getPrintParameters().copy(clientTransactionId = paymentResult.clientTransactionId),
                     result = lastTransactionResult
                 )
             }
         }
     }

    private fun setupChargeCard() {
        mBinding.buttonChargeCard.setOnClickListener {
            val amountStr = mBinding.chargeField.editText?.text.toString()
            if (amountStr.isNotBlank()) {
                // Convert amount to cents
                val amount: Long = (amountStr.toDouble() * 100).toLong()
                val receiptNumberStr = UUID.randomUUID().toString()
                mBinding.receiptNumber?.value = receiptNumberStr
                val userInfo = HashMap<String, Any>().apply {
                    this["UserId"] = "test01"
                    this["Title"] = "Manager"
                }

                val params = PaymentParameters(
                    receiptDelegate = receiptDelegate,
                    userInfo = userInfo,
                    staffMember = YocoStaff(name = "John", staffNumber = "007"),
                    note = "Test note",
                    billId = "test-bill-id",
                    transactionCompleteNotifier = { _: Int, result: PaymentResult ->
                        val transactionId = result.transactionId
                        // Do something once the payment is complete
                        refundTransactionId.value = transactionId
                    },
                    receiptNumber = receiptNumberStr
                )

                // Access SDK
                val clientTransactionId = YocoSDK.charge(
                    // required parameters
                    context = requireContext(),
                    amountInCents = amount,
                    paymentType = PaymentType.CARD,
                    currency = SupportedCurrency.ZAR,
                    tippingConfig = this.tippingConfig,
                    // optional: parameters
                    paymentParameters = params,
                    printParameters = getPrintParameters(),
                    result = chargeResult
                )
            } else {
                Toast.makeText(context, "Amount cannot be empty", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setupTippingConfig() {
        mBinding.radioGroupTips?.setOnCheckedChangeListener { group, checkedId ->
            tippingConfig = when (checkedId) {
                R.id.radio_no_tip -> TippingConfig.DO_NOT_ASK_FOR_TIP
                R.id.radio_ask_for_tip -> TippingConfig.ASK_FOR_TIP_ON_CARD_MACHINE
                R.id.radio_tip_param -> TippingConfig.INCLUDE_TIP_IN_AMOUNT(200)
                else -> TippingConfig.DO_NOT_ASK_FOR_TIP
            }
        }
    }

    private fun setupRefund() {
        mBinding.refundTransactionId = refundTransactionId
        mBinding.buttonRefund.setOnClickListener {
            val transactionIdStr = refundTransactionId.value
            if (!transactionIdStr.isNullOrBlank()) {
                val userInfo = HashMap<String, Any>().apply {
                    this["UserId"] = "test01"
                    this["Title"] = "Manager"
                }

                val params = RefundParameters(
                    amountInCents = 500,
                    staffMember = YocoStaff(name = "John", staffNumber = "007"),
                    receiptDelegate = receiptDelegate,
                    userInfo = userInfo,
                    refundCompleteNotifier = { _: Int, _: PaymentResult ->
                        // Do something once the refund is complete
                        refundTransactionId.value = ""
                    }
                )

                // Access SDK
                YocoSDK.refund(
                    context = requireContext(),
                    transactionId = transactionIdStr,
                    refundParameters = params,
                    result = refundResult,
                    printParameters = getPrintParameters().copy(
                        clientTransactionId = transactionIdStr,
                        transactionType = TransactionType.REFUND
                    )
                )
            } else {
                Toast.makeText(context, "Transaction Id cannot be empty", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setupLogout() {
        mBinding.buttonLogout.setOnClickListener {
            YocoSDK.logout(requireContext())
        }
    }

    private fun setupPairCardMachine() {
        mBinding.buttonConnect.setOnClickListener {
            YocoSDK.pairTerminal(context = requireContext(), result = pairingResult)
        }
    }

    private fun setupPrintProvider() {
        mBinding.radioGroupPrintProvider.setOnCheckedChangeListener { _, checkedId ->
            this.printerConfig.printerProvider = when (checkedId) {
                R.id.radio_print_provider_none -> {
                    mBinding.switchSuccess.visibility = View.GONE
                    PrinterProvider.NONE
                }
                R.id.radio_print_provider_external -> {
                    mBinding.switchSuccess.visibility = View.VISIBLE
                    PrinterProvider.EXTERNAL
                }
                else -> {
                    mBinding.switchSuccess.visibility = View.GONE
                    PrinterProvider.SDK
                }
            }
            YocoSDK.setPrinterConfig(this.printerConfig)
        }
    }

    private fun setupPrintSuccessSwitch() {
        mBinding.switchSuccess.isChecked = printSuccess
        mBinding.switchSuccess.setOnCheckedChangeListener { _, checked ->
            printSuccess = checked
        }
    }

    private fun validate(input: String?): Boolean {
        return !input.isNullOrBlank()
    }

    private fun printStartListener(paymentResult: PaymentResult, progress: UpdatePrintProgress) {
        lifecycleScope.launch {
            delay(1000)
            progress(PrintState.InProgress("Printing"))
            delay(1000)
            if (printSuccess) {
                progress(PrintState.Complete())
            } else {
                progress(PrintState.Error("No printer"))
            }
        }
    }

    private fun printerStatusUpdate(status: PrinterStatus) {
        println("Print Update status: $status")
    }

    private fun printResultUpdate(result: PrintResult) {
        println("Print Update result: $result")
    }
}
