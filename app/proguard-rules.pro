# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

-dontwarn org.slf4j.impl.StaticLoggerBinder
# Appcompat and support
-keep interface android.support.v7.** { *; }
-keep class android.support.v7.** { *; }
-keep interface android.support.v4.** { *; }
-keep class android.support.v4.** { *; }
-dontwarn android.app.Notification

# Gson
-keep interface com.google.gson.** { *; }
-keep class com.google.gson.** { *; }

# Retrofit
-keep class com.google.inject.** { *; }
-keep class org.apache.http.** { *; }
-keep class org.apache.james.mime4j.** { *; }
-keep class javax.inject.** { *; }
-keep class retrofit.** { *; }
-keep interface retrofit.** { *; }
-dontwarn rx.**
-dontwarn com.google.appengine.api.urlfetch.**
-dontwarn okio.**

-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**


# okhttp

-keepattributes Signature
-keepattributes *Annotation
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

#mpos.android.core
-keep class com.fasterxml.** { *; }
-dontwarn com.fasterxml.**
-keep class io.mpos.** { *; }
-dontwarn kotlinx.android.parcel.Parcelize

# logging
-keep class com.yoco.ono.android.logging.data.** { *; }

-dontwarn org.yaml.snakeyaml.**

-keep class com.google.android.material.** { *; }

-keep @kotlinx.serialization.Serializable class *

-dontwarn com.google.android.material.**
-dontnote com.google.android.material.**

-keep class com.birbit.android.jobqueue.** { *; }
-dontwarn com.birbit.android.jobqueue.scheduling.Gcm*
# Bluetooth card readers
-keep class com.yoco.ono.common.miura.** { *; }
-keep class com.yoco.ono.android.miura.** { *; }
-keep class com.yoco.ono.common.dspread.** { *; }
-keep class com.yoco.ono.android.dspread.** { *; }
-keep class com.yoco.ono.common.datecs.** { *; }
-keep class com.yoco.ono.android.datecs.** { *; }
-keep class com.yoco.ono.common.bluetooth.** { *; }
-keep class com.yoco.ono.android.bluetooth.** { *; }

# Cloud card readers
-keep class com.yoco.ono.common.cloud.** { *; }
-keep class com.yoco.ono.android.cloud.** { *; }

-keep class com.yoco.ono.common.modules.sdk.models.** { *; }

# Kozen integrated
-keep class com.yoco.ono.android.terminal.common.** { *; }
-keep class com.yoco.ono.android.** { *; }
-dontwarn com.pos.sdk.**